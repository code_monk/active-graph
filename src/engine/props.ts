import { EntityRef } from "./entity"

/**
 * PropertyBag represents a map of key-value pairs where they keys are strings and the values are anything at all.
 * It can attach to any [Entity] on the [Graph] (so either a [Vertex] or an [Edge]).
 * Your implementation will probably want to extend this and create some required fields.
 */
//export type PropertyBag = Map<string, any>


/**
 * a PropertyBag is a collection of key-value pairs.
 * There should be at most one per Entity
 */
export class PropertyBag extends Map<string, any>{
    toObject(): object {
        const obj: { [key: string]: any } = {};
        for (const [key, value] of this) {
            obj[key] = value;
        }
        return obj;
    }
}

/**
 * a PropertyBagStore maps an entity (an Edge or Vertex) to a PropertyBag.
 * There should be one per Graph.
 */
export type PropertyBagStore = Map<EntityRef, PropertyBag>
