import { Entity, EntityRef } from "./entity"
import { PropertyBag } from "./props"
import { Graph, Subgraph } from "./graph"
import { Path } from "./path"
import { Edges } from "./edge"

/**
 * A Vertex is an [Entity] on the Graph.
 * It can contain properties (via PropertyBag) and be associated to another Vertex via an [Edge]
 */
export interface Vertex extends Entity {
	ref: VertexRef
	props: VertexPropertyBag
	graph : Graph
	Edges() : {incoming: Edges, outgoing: Edges}
	IsAdjascentTo(ref: VertexRef) : boolean
	DistanceTo(ref: VertexRef) : number
	Neighbourhood() : Subgraph
	Degree() : number
	Centrality() : number
	Eccentricity() : boolean
	Betweenness() : number	
	Reciprocity() : number
	PathTo(otherVertex : VertexRef) : Path
}

/**
 * Vertices is the plural of [Vertex]
 */
export type Vertices = Map<VertexRef, Vertex>

/**
 * a VertexRef is a pointer (reference) to a Vertex
 */
export type VertexRef = EntityRef

/**
 * VertexRefs is a Set of VertexRef
 */
export type VertexRefs = Set<VertexRef>

/**
 * a VertexPropertyBag holds a bunch of data for a [Vertex]
 */
interface VertexPropertyBag extends PropertyBag{}