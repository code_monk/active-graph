import { Vertex, VertexRef } from "./vertex"
import { PropertyBag } from "./props"
import { Graph } from "./graph"

/**
 * an EdgeRef is a pointer (reference) to an Edge
 */
export type EdgeRef = Symbol

export type EdgeRefs = Set<EdgeRef>

/**
 * EdgePropertyBag is a [PropertyBag] that applies specifically to an [Edge]
 */
interface EdgePropertyBag extends PropertyBag{}

/**
 * an Edge connects one [Vertex] to another.
 * It also contains an [EdgePropertyBag] which may hold meaningful data, or be empty.
 */
export interface Edge {
	ref: EdgeRef
	from : VertexRef
	to : VertexRef
	props: EdgePropertyBag
	graph : Graph
	From(): Vertex
	To(): Vertex
}

/**
 * Edges is plural of [Edge].
 */
export type Edges = Map<EdgeRef, Edge>