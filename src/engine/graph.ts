import { Vertices } from "./vertex"
import { Edges } from "./edge"
import { Query, Mutation, Result } from "./query"
import { PropertyBag } from "./props"

/**
 * a Graph has 1 or more Vertices and 0 or more Edges connecting them.
 * You can query a Graph for Vertices, Edges, or a Subgraph.
 * @property state.vertices is a Map<VertexRef, Vertex>
 * @property state.edges is a Map<EdgeRef, Edge>
 * @method Edges returns Edges
 * @method Subgraph returns a Subgraph
 * @method Mutate modifies the Graph in place, returning the number of Entities affected
 */
export interface ReadableGraph {
	Edges() : Edges
	Vertices() : Vertices
	Query<ReturnType>(q : Query) : Result<ReturnType>
	Order() : number
	Size() : number
	Props() : PropertyBag

	//Density() : Result<number>
	//Isomorphic(otherGraph : ReadableGraph) : Result<boolean>
	//Diameter() : Result<number>
	//Connectedness() : Result<ConnectednessValue>
	//Strength() : Result<number>
	//Regularity() : Result<boolean>
}

/**
 * A Graph is a ReadableGraph that can be mutated
 */
export interface Graph extends ReadableGraph {
	Mutate<ReturnType>(m : Mutation) : Result<ReturnType>
}

/**
 * a Subgraph is just a Graph. Sometimes preferrable for readability.
 */
export type Subgraph = ReadableGraph


/**
 * a Graph's connectedness can only be one of these 4 values
 */
export enum ConnectednessValue {
	Disconnected,
	WeaklyConnected,
	SemiConnected,
	StronglyConnected
}