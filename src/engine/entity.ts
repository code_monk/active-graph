import { PropertyBag } from "./props"
import { Vertices } from "./vertex"
import { Edges } from "./edge"
import { Graph } from "./graph"

/**
 * An Entity is a member of a [Graph], so either an [Edge] or a [Vertex].
 * @property [ref] is a pointer. A way to refer to this object.
 * @property [props] is a [PropertyBag], which is a loose collection of key-value pairs
 * @property [grap] is a reference to the Graph it belongs to.
 */
export interface Entity {
	ref: EntityRef
	props: PropertyBag
	graph: Graph
}

/**
 * Entities holds all the Vertices and Edges of a Graph.
 * It can also hold anything else to make queries more performant or expressive
 */
export interface Entities {
	vertices : Vertices
	edges : Edges
	[k : string] : any
}


/**
 * An EntityRef is a pointer to (ie: a reference to) an [Entity].
 * So in practice a pointer to either an Edge or a Vertex.
 */
export type EntityRef = Symbol