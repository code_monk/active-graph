import { Graph } from "../engine/graph";
import { EdgeRef } from "../engine/edge";
import { VertexRef } from "../engine/vertex";
import { Query, Result } from "../engine/query";
import { Entities } from "../engine/entity";
import { ActivePropertyBag } from "./props";
import { VertexRefs } from "../engine/vertex";
import { EdgeRefs } from "../engine/edge";
import { PropertyBagStore } from "../engine/props";
import { ActiveVertex, ActiveVertices } from "./vertex";
import { ActiveEdge, ActiveEdges } from "./edge";
import { Visualizer } from "../vis/visualizer";

/**
 * Our data structure that will power the [Graph] interface.
 * Vertices and Edges are important. Everything else is for speed
 */
interface ActiveGraphState extends Entities {
	vertices: ActiveVertices
	edges: ActiveEdges
	vertexRefs: VertexRefs
	edgeRefs: EdgeRefs
	entityProps: PropertyBagStore
	props: ActivePropertyBag
	counter : bigint
}

/**
 * An ActiveGraph issues DOM Events when interesting things happen
 */
class ActiveGraph extends EventTarget implements Graph {
    private state : ActiveGraphState;
	viz : Visualizer;
    constructor(viz : Visualizer) {
        super();
        this.state = {
            vertices: new Map(),
            edges: new Map(),
            vertexRefs: new Set(),
            edgeRefs: new Set(),
            entityProps: new Map(),
            props: new ActivePropertyBag(this, "graph"),
            counter: 0n
        }
		this.viz = viz;
    }
    trigger(eventLayer : string, action : string, payload : any) : boolean {
        const detail = {eventType: action, payload};
        const ev = new CustomEvent(`activegraph/${eventLayer}`, { detail });
        return this.dispatchEvent(ev);
    }
	on(eventLayer : string, fn : EventListener) {
		this.addEventListener(`activegraph/${eventLayer}`, fn, {
			capture: true,
			passive: true
		});
	}
	Props() : ActivePropertyBag {
		return this.state.props;
	}
    createVertex(bag : object) : ActiveVertex {
        const vert = new ActiveVertex(this, bag);
        this.insertVertex(vert);
        return vert;
    }
    
    insertVertex(vert : ActiveVertex) {
		if (this.state.vertexRefs.has(vert.ref)) {
			throw Error("vertex already exists");
		} else {
			this.state.vertices.set(vert.ref, vert);
			this.state.vertexRefs.add(vert.ref);
			vert.props.set("i", ++this.state.counter);
			this.state.entityProps.set(vert.ref, vert.props);
            this.trigger("entity/mutation", "vertex/insert", {vertex: vert});
		}
	}
    updateVertex(vert : ActiveVertex) {
		if (!this.state.vertexRefs.has(vert.ref)) {
			throw Error("vertex doesn't exist");
		} else {
			this.state.vertices.set(vert.ref, vert);
			this.state.entityProps.set(vert.ref, vert.props);
            this.trigger("entity/mutation", "vertex/update", {vertex: vert});
		}
	}
    upsertVertex(vert : ActiveVertex) {
		if (this.state.vertexRefs.has(vert.ref)) {
			this.updateVertex(vert);
		} else {
			this.insertVertex(vert);
		}
	}
    deleteVertex(ref : VertexRef) : boolean {
		if (this.state.vertexRefs.has(ref)) {
			//	deleting a vertex automatically deletes attached edges
			const numberofEdgesDeleted = this.disconnectVertex(ref);
			this.state.vertexRefs.delete(ref);
			this.state.vertices.delete(ref);
			this.state.entityProps.delete(ref);
            this.trigger("entity/mutation", "vertex/delete", {ref, numberofEdgesDeleted});
			return true;
		} else {
			return false;
		}
	}
    getVertex(ref: VertexRef): ActiveVertex {
		return this.state.vertices.get(ref) as ActiveVertex;
	}
	getEdge(ref : EdgeRef) : ActiveEdge {
		return this.state.edges.get(ref) as ActiveEdge;
	}
    createEdge(bag : object, from : VertexRef, to : VertexRef) : ActiveEdge {
        const edge = new ActiveEdge(this, from, to, bag);
        this.insertEdge(edge);
        this.trigger("entity/mutation", "edge/create", {edge});
        return edge;
    }
    insertEdge(e : ActiveEdge) {
		if (this.state.edges.has(e.ref)) {
			throw Error("edge already exists");
		} else {
			this.state.edges.set(e.ref, e);
			this.state.edgeRefs.add(e.ref);
			e.props.set("i", ++this.state.counter);
			this.state.entityProps.set(e.ref, e.props);
            this.trigger("entity/mutation", "edge/insert", {edge: e});
		}
	}
    updateEdge(e : ActiveEdge) {
		if (!this.state.edges.has(e.ref)) {
			throw Error("edge does not exist");
		} else {
			this.state.edges.set(e.ref, e);
			this.state.edgeRefs.add(e.ref);
			this.state.entityProps.set(e.ref, e.props);
            this.trigger("entity/mutation", "edge/update", {edge: e});
		}
	}
    upsertEdge(e : ActiveEdge) {
		if (this.state.edges.has(e.ref)) {
			this.updateEdge(e);
		} else {
			this.insertEdge(e);
		}
	}
    deleteEdge(ref : EdgeRef) : boolean {
		if (this.state.edgeRefs.has(ref)) {
			this.state.edgeRefs.delete(ref);
			this.state.edges.delete(ref);
			this.state.entityProps.delete(ref);
            this.trigger("entity/mutation", "edge/delete", {ref});
			return true;
		} else {
			return false;
		}
	}
    disconnectVertex(ref : VertexRef, removeIncoming = true, removeOutgoing = true, loudly = true) : number {
		//	remove edges attached to a vertex
		//	returns the number of edges removed
		if (!removeIncoming && !removeOutgoing) {
			//	no op
			return 0
		}
		let n = 0;
		const {incoming, outgoing} = this.getVertex(ref)?.Edges() || {incoming: new Map(), outgoing: new Map()};
		if (removeIncoming) {
			for (ref of incoming.keys()) {
				this.deleteVertex(ref);
			}
			n = n + incoming.size;
		}
		if (removeOutgoing) {
			for (ref of outgoing.keys()) {
				this.deleteVertex(ref);
			}
			n = n + outgoing.size;
		}
        if (n > 0) {
            this.trigger("entity/mutation", "vertex/disconnect", {ref});
        }
		return n;
	}
    Edges() : ActiveEdges {
        return this.state.edges;
    }
    Vertices(): ActiveVertices {
        return this.state.vertices;
    }
	getVertexRefByIndex(i : number) : VertexRef {
		//	get a vertexRef by index
		return [...this.state.vertexRefs][i];
	}
	getEdgeRefByIndex(i : number) : EdgeRef {
		return [...this.state.edgeRefs][i];
	}
    Order(): number {
        return this.Edges().size;
    }
    Size(): number {
        return this.Vertices().size;
    }
    Query<ReturnType>(q: Query): Result<ReturnType> {
        return q.bind(this, this.state)();
    }
    Mutate<ReturnType>(m: Query): Result<ReturnType> {
        return m(this.state);
    }
}

export { ActiveGraph }