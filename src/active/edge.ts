import { Edge, EdgeRef } from "../engine/edge";
import { newUniqueId } from "../engine/util";
import { VertexRef } from "../engine/vertex";
import { ActiveGraph } from "./graph";
import { ActivePropertyBag } from "./props";
import { ActiveVertex as ActiveVertex } from "./vertex";

export type ActiveEdges = Map<EdgeRef, ActiveEdge>


/**
 * An EventedEdge holds an ActivePropertyBag, which contains
 * a reference to the parent Graph, where it will despatch events
 */
export class ActiveEdge implements Edge  {
    ref : EdgeRef;
    from : VertexRef;
    to : VertexRef;
    props : ActivePropertyBag;
    graph : ActiveGraph;
    constructor(graph : ActiveGraph, from : VertexRef, to : VertexRef, bag : object | ActivePropertyBag) {
        const id = newUniqueId("edge");
        this.ref = Symbol(id);
        this.from = from;
        this.to = to;
        this.graph = graph;
        if (bag instanceof ActivePropertyBag) {
            this.props = bag;
        } else {
            this.props = ActivePropertyBag.fromObject(bag, graph, id);
        }
        this.props.set("id", id);
    }
    From() : ActiveVertex {
        return this.graph.getVertex(this.from) as ActiveVertex;
    }
    To() : ActiveVertex {
        return this.graph.getVertex(this.to) as ActiveVertex;
    }
}