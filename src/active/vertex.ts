import { Edges as ActiveEdges } from "../engine/edge";
import { ReadableGraph } from "../engine/graph";
import { Path } from "../engine/path";
import { Vertex, VertexRef } from "../engine/vertex";
import { newUniqueId } from "../engine/util";
import { ActiveGraph } from "./graph";
import { ActivePropertyBag } from "./props";


/**
 * Vertices is the plural of [Vertex]
 */
export type ActiveVertices = Map<VertexRef, ActiveVertex>

/**
 * an EventedVertex holds its properties in an EventedPropertyBag,
 * which despatches mutation events to the parent Graph.
 */
export class ActiveVertex implements Vertex  {
    graph: ActiveGraph;
    ref: VertexRef;
    props : ActivePropertyBag;
    constructor(graph : ActiveGraph, bag : ActivePropertyBag | object) {
        const id = newUniqueId("vertex");
        this.ref = Symbol(id);
        this.graph = graph;
        if (bag instanceof ActivePropertyBag) {
            this.props = bag;
        } else {
            this.props = ActivePropertyBag.fromObject(bag, graph, id);
        }
        this.props.set("id", id);
    }
    Edges(): { incoming: ActiveEdges; outgoing: ActiveEdges; } {
        const incoming : ActiveEdges = new Map();
        const outgoing : ActiveEdges = new Map();
        this.graph.Edges().forEach((edge, edgeRef) => {
            if (edge.from === this.ref) {
                outgoing.set(edgeRef, edge);
            }
            if (edge.to === this.ref) {
                incoming.set(edgeRef, edge);
            }
        });
        return {incoming, outgoing};
    }
    IsAdjascentTo(ref: VertexRef): boolean {
        const edges = this.Edges();
        edges.incoming.forEach(edge => {
            if (edge.from === ref) {
                return true;
            }
        });
        edges.outgoing.forEach(edge => {
            if (edge.to === ref) {
                return true;
            }
        });
        return false;
    }
    DistanceTo(ref: Symbol): number {
        throw Error("not implemented");
    }
    Neighbourhood(): ReadableGraph {
        throw Error("Not implemented");
    }
    Degree(): number {
        throw Error("not implemented");
    }
    Centrality(): number {
        throw Error("not implemented");
    }
    Eccentricity(): boolean {
        throw Error("not implemented");
    }
    Betweenness(): number {
        throw Error("not implemeneted");
    }
    Reciprocity(): number {
        throw Error("not implemented");
    }
    PathTo(otherVertex: Symbol): Path {
        throw Error("not implemented");
    }

}