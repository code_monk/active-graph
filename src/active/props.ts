import { UniqueId } from "../engine/util";
import { ActiveGraph } from "./graph";

/**
 * an EventedPropertyBag is a Map<string, any> that firss an event whenever a mutation occurs
 * it dispatches those events to its "bubbleTarget" (usually a Vertex, Edge, or Graph)
 * namespace is appended to the event name.
 * @example const bag = new EventedPropertyBag(myVertex, "vertex");
 * @example bag.set("hello", "world");
 * @example fires CustomEvent('hypergraph', { "eventType": "vertex/set", "payload": { "k": "hello", "v": "world" }})
 */
export class ActivePropertyBag extends Map<string, any> {
    graph: ActiveGraph
    entityId: UniqueId;
    constructor(eventTarget: ActiveGraph, namespace: string) {
        super();
        this.graph = eventTarget;
        this.entityId = namespace;
    }
    toObject(): object {
        const obj: { [key: string]: any } = {};
        for (const [key, value] of this) {
            obj[key] = value;
        }
        return obj;
    }
    static fromObject(obj: Object, eventTarget: ActiveGraph, namespace: string) {
        const bag = new ActivePropertyBag(eventTarget, namespace);
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                bag.set(key, obj[key]);
            }
        }
        return bag;
    }
    set(k: any, v: any): this {
        super.set(k, v);
        this.trigger("set", { k, v });
        return this
    }
    delete(k: any): boolean {
        const somethingHappened = super.delete(k);
        if (somethingHappened) {
            this.trigger("delete", { k });
        }
        return somethingHappened;
    }
    clear(): void {
        this.trigger("clear", { numberOfRecords: this.size });
        return super.clear();
    }
    trigger(action: string, payload: any) {
        payload = {
            prop: payload,
            entity: {
                id: this.entityId
            }
        }
        this.graph.trigger("prop/mutation", action, payload);
    }
}