import { ActiveEdge } from "../active/edge";
import { ActiveGraph } from "../active/graph";
import { ActiveVertex } from "../active/vertex";
import { Visualizer, VisLink, VisNode } from "./visualizer";
import { Color, NodeShapeType, Orb, OrbEventType } from '@memgraph/orb';

class OrbVisualizer implements Visualizer {
    engine: any
    constructor(domNode: HTMLElement) {
        this.engine = new Orb<VisNode, VisLink>(domNode);
    }
    async Initialize(activeGraph : ActiveGraph): Promise<any> {
        this.engine.view.setSettings({ simulation: { isPhysicsEnabled: true } });
        const nodes: VisNode[] = [];
        const links: VisLink[] = [];
        for (const [_, vertex] of activeGraph.Vertices()) {
            nodes.push(this.VertexToNode(vertex))
        }
        for (const [_, edge] of activeGraph.Edges()) {
            links.push(this.EdgeToLink(activeGraph, edge));
        }
        this.engine.data.setup({ nodes, links });
        this.engine.view.setSettings({ simulation: { isPhysicsEnabled: true } });
        this.engine.data.setup({ nodes, links });
        return true
    }
    VertexToNode(vert: ActiveVertex): VisNode {
        //  what properties do we want to copy over?
        //  what massaging and filtering do we want to do?
        //  if we copy properties over wholesale, what are the performance penalties?
        const id = vert.props.get("id");
        const label = vert.props.get("label");
        const color = vert.props.get("color");
        const shape = vert.props.get("shape");
        const size = vert.props.get("size");
        const i = vert.props.get("i");
        return { id, label, color, shape, size, i };
    }
    EdgeToLink(g : ActiveGraph, edge: ActiveEdge): VisLink {
        const id = edge.props.get("link");
        const label = edge.props.get("label");
        const color = edge.props.get("color");
        const i = edge.props.get("i");
        const start = g.getVertex(edge.to).props.get("id");
        const end = g.getEdge(edge.from).props.get("id");
        return { id, label, color, i, start, end };
    }
    Render(): Promise<true> {
        const orb = this.engine;
        return orb.view.render(() => {
            orb.view.recenter();
        });
    }
}

export { OrbVisualizer }