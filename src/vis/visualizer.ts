import { ActiveVertex } from "../active/vertex";
import { UniqueId } from "../engine/util";
import { ActiveEdge } from "../active/edge";
import { ActiveGraph } from "../active/graph"


/**
 * A VisNode represents your Vertex in a format suitable to your display engine
 */
export interface VisNode {
	id: UniqueId;
	label: string;
	color: string;
	shape: string;
	size: number;
    i: bigint;
	[k : string] : any
}

/**
 * a VisLink represents an edge in a format suitable for your disaplay engine
 */
export interface VisLink {
	id: UniqueId;
	label: string;
	color: string;
	start: UniqueId;
	end: UniqueId;
    i: bigint
    [k : string] : any
}

/**
 * Orb Visualizer implements Visualizer and renders an ActiveGraph to canvas
 */
export interface Visualizer {
    Render(g : ActiveGraph) : Promise<any>
	VertexToNode(vert : ActiveVertex) : VisNode
	EdgeToLink(g : ActiveGraph, edge : ActiveEdge) : VisLink
	Initialize(g : ActiveGraph) : Promise<any>
}

// const id = vert.props.get("id");
// const label = vert.props.get("label");
// const color = vert.props.get("color");
// const shape = vert.props.get("shape");
// const size = vert.props.get("size");
// const i = vert.props.get("i");
// return { id, label, color, shape, size, i };