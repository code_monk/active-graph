import { OrbVisualizer } from "./vis/orb";
import { ActiveGraph } from "./active/graph";

const graphNode = document.getElementById('graph');

if (graphNode !== null) {
    const viz = new OrbVisualizer(graphNode);
    const g = new ActiveGraph(viz);

    const bob = g.createVertex({"label": "Bob"});
    const alice = g.createVertex({"label": "Alice"});
    const david = g.createVertex({label: "david"});
    const bobLikesAlice = g.createEdge({"rel": "likes"}, bob.ref, alice.ref);
    const bobLikesDavid = g.createEdge({rel: "likes"}, bob.ref, david.ref);

    console.log(bobLikesAlice, bobLikesDavid);
    
    g.viz.Render(g);

} else {
    console.error("DOM node doesn't exist")
}
